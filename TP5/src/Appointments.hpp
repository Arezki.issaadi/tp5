#pragma once

#include <string>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <numeric>

#include "Constants.hpp"
#include "CSVHandler.hpp"
#include "AppointmentsData.hpp"

class Appointments
{
public:
    Appointments(std::string filePath);
    void display(unsigned int userCol, std::string userId, std::vector<unsigned int> displayedColumns = {});
    bool schedule(std::string patientId, std::string nurseId);
    int getHoursRate(std::string patientDisease, std::string nurseExperience);
    std::vector<std::vector<std::string>>* getData();
    void setData(std::vector<std::vector<std::string>> data);
    void fillDisplayedColumns(std::vector<unsigned int>& displayedColumns);
    void displayColumnsHeaders(std::vector<unsigned int>& displayedColumns);
    // 3. On filtre les rendez-vous pour ne conserver ceux de l'utilisateur passé en paramètre (soit un infirmier ou un patient):
    void filtrateUserAppointments(unsigned int userCol, std::string userId,   std::vector<std::vector<std::string>>& filteredAppointments);
    void displayFilteredAppointments( std::vector<std::vector<std::string>>& filteredAppointments, unsigned int& appointmentIdx, std::vector<unsigned int>& displayedColumns );




private:
    std::vector<std::string> createNewAppointment(std::string patientId, std::string nurseId);
    std::vector<std::string> _headers;
    CSVHandler _csvHandler;
    const unsigned int _COLUMN_WIDTH = 14;
    std::vector<std::vector<std::string>> _data;
};
