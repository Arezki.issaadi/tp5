#include "Appointments.hpp"

Appointments::Appointments(std::string filePath) : _csvHandler(CSVHandler(filePath))
{
    this->_headers = this->_csvHandler.getHeader(); // It is correct to return vector<T> as lvalue.
    this->_data = std::vector<std::vector<std::string>>();
    this->_data = (this->_csvHandler.getData());
}

void Appointments::fillDisplayedColumns(std::vector<unsigned int> &displayedColumns)
{
    if (displayedColumns.empty())
    {
        displayedColumns = std::vector<unsigned int>(this->_headers.size());
        std::iota(displayedColumns.begin(), displayedColumns.end(), 0);
    }
}

void Appointments::displayColumnsHeaders(std::vector<unsigned int> &displayedColumns)
{
    for (unsigned int col : displayedColumns)
    {
        std::cout << std::setw(this->_COLUMN_WIDTH)
                  << this->_headers[col];
    }
    std::cout << std::endl
              << std::string(displayedColumns.size() * this->_COLUMN_WIDTH, '_')
              << std::endl;
}
void Appointments::filtrateUserAppointments(unsigned int userCol, std::string userId, std::vector<std::vector<std::string>> &filteredAppointments)

{
    auto isUserWanted = [userId, userCol](std::vector<std::string> appointment) {
        return appointment[userCol] == userId;
    };
    std::copy_if(
        this->_data.begin(),
        this->_data.end(),
        std::back_inserter(filteredAppointments),
        isUserWanted);
}

void Appointments::displayFilteredAppointments(std::vector<std::vector<std::string>> &filteredAppointments, unsigned int &appointmentIdx, std::vector<unsigned int> &displayedColumns)
{
    for (std::vector<std::string> filteredAppointment : filteredAppointments)
    {
        std::cout << appointmentIdx++
                  << '.'
                  << std::setw(this->_COLUMN_WIDTH - 2);

        for (unsigned int col : displayedColumns)
        {
            std::cout << filteredAppointment[col].substr(0, this->_COLUMN_WIDTH - 1)
                      << std::setw(this->_COLUMN_WIDTH);
        }

        std::cout << '\n';
    }
}

void Appointments::display(unsigned int userCol, std::string userId, std::vector<unsigned int> displayedColumns)
{
    std::vector<std::vector<std::string>> filteredAppointments;
    unsigned int appointmentIdx = 1;

    // 1. On remplit le vecteur displayedColumns par tous les indexes des colonnes du fichier CSV:
    fillDisplayedColumns(displayedColumns);

    // 2. On affiche toutes les en-têtes des colonnes voulues du fichier CSV:
    displayColumnsHeaders(displayedColumns);
    // 3. On filtre les rendez-vous pour ne conserver ceux de l'utilisateur passé en paramètre (soit un infirmier ou un patient):
    filtrateUserAppointments(userCol, userId, filteredAppointments);
    // 4. On affiche ensuite les rendez-vous filtrés à l'écran:
    displayFilteredAppointments(filteredAppointments, appointmentIdx, displayedColumns);
    
}

bool Appointments::schedule(std::string patientId, std::string nurseId)
{
    std::vector<std::string> newAppointment = this->createNewAppointment(patientId, nurseId);

    for (std::vector<std::string> appointment : (this->_data))
    {
        bool isOccupied = newAppointment[APPOINTMENT_DATE] == appointment[APPOINTMENT_DATE] &&
                          newAppointment[APPOINTMENT_TIME] == appointment[APPOINTMENT_TIME];
        if (isOccupied)
        {
            return false; // Schedule did not work
        }
    }
    // Update appointment list and file.
    this->_data.push_back(newAppointment);
    this->_csvHandler.writeData(newAppointment);
    return true;
}

int Appointments::getHoursRate(std::string patientDisease, std::string nurseExperience)
{
    int years = std::stoi(nurseExperience);
    int rate = 0;
    
    if (patientDisease == "covid-19")
    {
        return rate = 2;
    }

    

    rate = 20;
    switch (years){
        case(years<=2):  return rate;
                         break;
        case(years < 5): return rate * 1.5;
                         break;
        default:         return rate * 1.8;
                         break;

    }
    /*
    if (years <= 2)
    {
        return rate;
    }

    if (years < 5)
    {
        return rate * 1.5;
    }
    else
    {
        return rate * 1.8;
    }

   */ 
}

std::vector<std::string> Appointments::createNewAppointment(std::string patientId, std::string nurseId)
{
    std::vector<std::string> newAppointment(this->_headers.size());
    newAppointment[APPOINTMENT_NURSE_ID] = nurseId;
    newAppointment[APPOINTMENT_PATIENT_ID] = patientId;

    std::cout << "\nVeuillez indiquer la date du rendez-vous [jj/mm/aaaa]: ";
    std::cin >> newAppointment[APPOINTMENT_DATE];

    std::cout << "\nVeuillez indiquer le moment du rendez-vous [AM/PM]: ";
    std::cin >> newAppointment[APPOINTMENT_TIME];

    std::cout << "\nVeuillez indiquer la raison du rendez-vous: ";
    std::cin >> newAppointment[APPOINTMENT_REASON];

    return newAppointment;
}

std::vector<std::vector<std::string>> *Appointments::getData()
{
    return &this->_data;
}

void Appointments::setData(std::vector<std::vector<std::string>> data)
{
    this->_data = data;
}